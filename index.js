const { server, app } = require('./generateServer')();

const websockets = require('./websockets');
const websocketsEvents = require('./websockets/events');
const registerSocketHandlers = require('./websockets/registerHandlers');

const {userService, socketConnectionService} = require('./services')

const {ioSubscriber, ioEmitter} = websockets.createSocketConnection(server, { path: '/socket.io' });

ioSubscriber.on('connection', (socket) => {
    console.log('client connected', socket.id);
    setTimeout(() => {
        console.log('sending heartbeat to ', socket.id);
        socket.emit(websocketsEvents.HEARTBEAT, { hello: 'world' });
    }, 1000);
    // Initialize handlers
    registerSocketHandlers.registerHandlers(socket, require('./websocketsHandlers').handlers)
})

ioEmitter.redis.on('error', (err) => {
    console.log('Redis error, shutting down service', err);
    process.exit(1);
})

ioEmitter.redis.on('end', () => {
    console.log('Redis connection closed, shutting down service')
    process.exit(1);
})

app.get('/health', (req, res) => {
    return res.status(200).send('OK');
})

app.get('/socket.io', (req, res) => {
    res.status(200).send('OK');
})

app.post('/', (req, res) => {
    const { email, _id, room, event, data } = req.body;
    let q;
    if (email) {
        q = userService.find({ email });
    } else if (_id) {
        q = userService.find({ _id })
    } else if (room) {
        ioEmitter.to(room).emit(event, data)
        return res.json({ success: true });
    }
    q.then((users) => {
        if (!users || users.length === 0) throw new Error('Invalid user ');
        return socketConnectionService.find({ userId: users[0]._id })
    })
    .then((socketConnections) => {
        if (!socketConnections || socketConnections.length === 0) {
            return res.json({ online: false });
        }
        ioEmitter.to(socketConnections[0].socketId).emit(event, data) 
        return res.json({ success: true, online: true })
    })
    .catch(err => {
        console.log(err);
        return res.status(400).send(err.message);
    })
})


const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
