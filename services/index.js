const AUTH_SERVICE_API_ROOT = process.env.AUTH_SERVICE_API_ROOT;
const USER_SERVICE_API_ROOT = process.env.USER_SERVICE_API_ROOT;
const SOCKET_CONNECTION_SERVICE_API_ROOT = process.env.SOCKET_CONNECTION_SERVICE_API_ROOT;
const APIKEY_SERVICE_API_ROOT = process.env.APIKEY_SERVICE_API_ROOT;

const userService = require('@videowiki/services/user')(USER_SERVICE_API_ROOT);
const authService = require('@videowiki/services/auth')(AUTH_SERVICE_API_ROOT);
const apiKeyService = require('@videowiki/services/apiKey')(APIKEY_SERVICE_API_ROOT)

const socketConnectionService = require('@videowiki/services/socketConnection')(SOCKET_CONNECTION_SERVICE_API_ROOT);


module.exports = {
    userService,
    authService,
    socketConnectionService,
    apiKeyService,
}